# Для build образа используем образ Debian 9
FROM debian:9 as build

# В первом слое устанавливаем пакеты: 
#  - wget для скачивания исходников nginx
#  - make и gcc для компиляции и сборки
#  - libpcre3, libpcre3-dev, zlib1g и zlib1g-dev нужны для поддежки пакетов с которыми компилируется  nginx по умолчанию
RUN apt update && apt install -y wget make gcc libpcre3 libpcre3-dev zlib1g zlib1g-dev

# В следующем слое:
# - Скачиваем исходники nginx. На момент создания Dockerfile последней является версия 1.21.4
# - Распаковываем скачанный архив 
# - Переходим в распакованную папку
# - Запускаем создание Makefile
# - Собираем из исходников
# - Устанавливаем собранный файл по месту расположения по умолчанию (/usr/local/nginx/sbin), присваивая ему нужные права
RUN wget http://nginx.org/download/nginx-1.21.4.tar.gz && tar -xzvf nginx-1.21.4.tar.gz && cd nginx-1.21.4 && ./configure && make && make install

# Для окончательного образа также используем образ Debian 9
FROM debian:9

# Устанавливаем рабочую директорию
WORKDIR /usr/local/nginx/sbin

# Из образа, помеченного как build копируем директорию
COPY --from=build /usr/local/nginx/sbin .
# В данном слое создаем папки logs и conf, необходимые для работы,
# а так же файл mime.types, который требуется для конфигурации по умолчанию.
# Также делаем файл nginx исполняемым
RUN mkdir ../logs ../conf && touch ../conf/mime.types && chmod +x nginx

#Прописываем команду для запуска nginx при запуске контейнера
#В команде прописываем запуск nginx не в фоне, иначе nginx запустится, 
#запустит свой демон, а после закроется, что приведет к закрытию контейнера, несмотря на то что в фоне остается демон
CMD ["./nginx","-g","daemon off;"]