# JUNEWAY Less1.4
# @smirkon

## Описание задания

Необходимо собрать nginx из исходников со стандартными модулями и запустить бинарник во втором образе.

https://habr.com/ru/post/321468/

Образы Debian9

При запуске примаунтить конфиг в контейнер.

https://www.thegeekstuff.com/2011/07/install-nginx-from-source/
https://docs.docker.com/develop/develop-images/multistage-build/
https://docs.docker.com/storage/bind-mounts/



Задача для прохождения на 100% (выполнять не обязательно)
Добавить в сборку
https://github.com/openresty/lua-nginx-module


В ответе dockerfile в gitlab как отдельный проект

---
1. Создаем `Dockerfile`:

```dockerfile
# Для build образа используем образ Debian 9
FROM debian:9 as build

# В первом слое устанавливаем пакеты: 
#  - wget для скачивания исходников nginx
#  - make и gcc для компиляции и сборки
#  - libpcre3, libpcre3-dev, zlib1g и zlib1g-dev нужны для поддежки пакетов с которыми компилируется  nginx по умолчанию
RUN apt update && apt install -y wget make gcc libpcre3 libpcre3-dev zlib1g zlib1g-dev

# В следующем слое:
# - Скачиваем исходники nginx. На момент создания Dockerfile последней является версия 1.21.4
# - Распаковываем скачанный архив 
# - Переходим в распакованную папку
# - Запускаем создание Makefile
# - Собираем из исходников
# - Устанавливаем собранный файл по месту расположения по умолчанию (/usr/local/nginx/sbin), присваивая ему нужные права
RUN wget http://nginx.org/download/nginx-1.21.4.tar.gz && tar -xzvf nginx-1.21.4.tar.gz && cd nginx-1.21.4 && ./configure && make && make install

# Для окончательного образа также используем образ Debian 9
FROM debian:9

# Устанавливаем рабочую директорию
WORKDIR /usr/local/nginx/sbin

# Из образа, помеченного как build копируем директорию
COPY --from=build /usr/local/nginx/sbin .
# В данном слое создаем папки logs и conf, необходимые для работы,
# а так же файл mime.types, который требуется для конфигурации по умолчанию.
# Также делаем файл nginx исполняемым
RUN mkdir ../logs ../conf && touch ../conf/mime.types && chmod +x nginx

#Прописываем команду для запуска nginx при запуске контейнера
#В команде прописываем запуск nginx не в фоне, иначе nginx запустится, 
#запустит свой демон, а после закроется, что приведет к закрытию контейнера, несмотря на то что в фоне остается демон
CMD ["./nginx","-g","daemon off;"]
```

2. Запускаем создание сконфигурированного образоа: 
``` bash
docker build -t less1.4:v1 .
```

3. В текущую папку помещаем конфиг `nginx.conf` из поставки nginx.

4. Запускаем контейнер из созданного образа, монтируя `nginx.conf` в контейнер в каталог конфигураций nginx. Так же объявляем перенаправление портов - с локального 8001 на порт 80 в контейнере:
```bash
 docker run -d -p 8001:80 -v $(pwd)/nginx.conf:/usr/local/nginx/conf/nginx.conf less1.4:v1
 ```
 5. Проверяем, что nginx работает:
 ```bash
$ curl http://localhost:8001

<html>
<head><title>404 Not Found</title></head>
<body>
<center><h1>404 Not Found</h1></center>
<hr><center>nginx/1.21.4</center>
</body>
</html>
 ```